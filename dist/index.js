'use strict';

var _scraper = require('./services/scraper.js');

var express = require('express');
var bodyParser = require('body-parser');
var _ = require('underscore');
var db = require('./db.js');
var bcrypt = require('bcrypt');
var middleware = require('./middleware.js')(db);


var app = express();
var PORT = process.env.PORT || 3000;

var item = {
  price: 0,
  title: "title",
  brand: "brand",
  sku: "1234",
  url: "http://test.com",
  category: "Jackets"
};

// GET /categories
app.use(bodyParser.json());

app.get('/', function (req, res) {

  //scrape(db);
  // db.sequelize.query("SELECT DISTINCT price FROM products", { type: db.sequelize.QueryTypes.SELECT})
  // .then(function(prices) {
  //   console.log(prices)
  //   res.send("Success!")
  //   // We don't need spread here, since only the results will be returned for select queries
  // })
  res.send("What up");
});

// POST /users
app.post('/users', function (req, res) {
  var body = _.pick(req.body, 'email', 'password');

  db.user.create(body).then(function (user) {
    return res.json(user.toPublicJSON());
  }, function (e) {
    res.status(400).json(e);
  });
});

// POST /users/login
app.post('/users/login', function (req, res) {
  var body = _.pick(req.body, 'email', 'password');
  var userInstance;

  db.user.authenticate(body).then(function (user) {
    var token = user.generateToken('authentication');
    userInstance = user;
    return db.token.create({
      token: token
    });
  }).then(function (tokenInstance) {
    res.header('Auth', tokenInstance.get('token')).json(userInstance.toPublicJSON());
  }).catch(function () {
    res.status(401).send();
  });
});

//  DELETE /users/login
app.delete('/users/login', middleware.requireAuthentication, function (req, res) {
  req.token.destroy().then(function () {
    res.status(204).send();
  }).catch(function () {
    res.status(500).send();
  });
});

db.sequelize.sync({
  //force:true
}).then(function () {
  app.listen(PORT, function () {
    console.log('Express server started at: http://localhost:' + PORT);
  });
}).catch(function (e) {
  return console.log(e);
});
//# sourceMappingURL=index.js.map