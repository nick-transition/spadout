'use strict';

module.exports = function (sequelize, DataTypes) {
  return sequelize.define('company', {
    name: {
      type: DataTypes.STRING,
      allowNull: false,
      unique: true,
      validate: {
        len: [2, 240]
      }
    }
  });
};
//# sourceMappingURL=company.js.map