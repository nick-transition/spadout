'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

exports.default = function (sequelize, DataTypes) {
  return sequelize.define('token', {
    token: {
      type: DataTypes.VIRTUAL,
      allowNUll: false,
      validate: {
        len: [1]
      },
      set: function set(value) {
        var hash = cryptojs.MD5(value).toString();

        this.setDataValue('token', value);
        this.setDataValue('tokenHash', hash);
      }
    },
    tokenHash: DataTypes.STRING
  });
};

var cryptojs = require('crypto-js');

;
//# sourceMappingURL=token.js.map