'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
var request = require('request');
var rp = require('request-promise');
var cheerio = require('cheerio');
var Promise = require('bluebird');
var _ = require('underscore');

var itemPageQue = [];

function crawlSitemap(url, itemSelector) {
  return rp(url).then(function (html) {
    var $ = cheerio.load(html);
    var cat = void 0,
        title = void 0,
        link = void 0;
    var pageList = [];

    $(itemSelector).each(function (i, elem) {
      var str = $(elem).prev('h3').text();
      cat = str.slice(0, str.length - 1); // remove trailing :

      $(elem).find('a').each(function (j, el) {
        var obj = { category: "", linkTitle: "", link: "" };
        obj.category = cat;
        obj.linkTitle = $(el).text();
        obj.link = "http://www.moosejaw.com/moosejaw/shop/" + $(el).attr('href');

        pageList.push(obj);
      });
    });
    return pageList;
  });
}

function getItemDetails(db, page) {
  var item = {
    price: 0,
    title: "",
    brand: "",
    sku: "",
    url: "",
    category: ""
  };

  return rp(page.link).then(function (html) {
    var $ = cheerio.load(html);
    item.price = $('input#adwordsTotalValue').first().attr('value');
    item.title = $('h1.pdp-main-ttl').text().trim();
    item.url = page.link;
    item.brand = $('meta[itemprop=brand]').attr('content');
    item.sku = $('meta[itemprop=sku]').attr('content');
    item.category = page.category;
    db.company.findOne({ where: { name: "Moosejaw" } }).then(function (company) {
      db.product.create(item).then(function (item) {
        company.addProduct(item).then(function (product) {
          return product.reload();
        }).catch(function (e) {
          return console.log(e);
        });
      }).catch(function (e) {
        return console.log(e);
      });
    }).catch(function (e) {
      return console.log(e);
    });
    return item;
  });
}

// Iterate through each page of items collecting item urls as we go
function crawlSiteLoop(pageObj) {
  return rp(pageObj.link).then(function (html) {
    var $ = cheerio.load(html);
    console.log(pageObj.link);
    $('a.pdpLink').each(function (i, elem) {
      if (!_.contains(itemPageQue, elem)) {
        itemPageQue.push({
          'link': $(elem).attr('href'),
          'category': pageObj.category
        });
      }
      return itemPageQue;
    });
    var nextPage = $('a.icon-paging-right').first().attr('href');
    if (nextPage) {
      pageObj.link = nextPage;
      return new Promise(function (resolve, reject) {
        resolve(crawlSiteLoop(pageObj));
      });
    } else {
      return itemPageQue;
    }
  });
}

function crawlCatz(catz) {
  return Promise.each(catz, function (cat) {
    return crawlSiteLoop(cat);
  }).then(function () {
    return itemPageQue;
  });
}

exports.crawlSitemap = crawlSitemap;
exports.crawlCatz = crawlCatz;
exports.getItemDetails = getItemDetails;
//# sourceMappingURL=crawlerMJ.js.map