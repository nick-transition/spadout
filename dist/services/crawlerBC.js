'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
var request = require('request');
var rp = require('request-promise');
var cheerio = require('cheerio');

var itemUrls = [];
var itemList = [];
var category = void 0;

function crawlSitemap(url, itemSelector) {
  return rp(url).then(function (html) {
    var $ = cheerio.load(html);
    var cat = void 0,
        title = void 0,
        link = void 0;
    var pageList = [];

    $(itemSelector).each(function (i, elem) {
      var str = $(elem).prev('h3').text();
      var isSale = $(elem).prev('h2').text();
      if (isSale == "On Sale") {
        console.log("On Sale");
      }
      cat = str.slice(0, str.length - 1); // remove trailing :

      $(elem).find('a').each(function (j, el) {
        var obj = { category: "", linkTitle: "", link: "" };
        obj.category = cat;
        obj.linkTitle = $(el).text();
        obj.link = "http://www.moosejaw.com/moosejaw/shop/" + $(el).attr('href');
        pageList.push(obj);
      });
    });
    return pageList;
  });
}

function getItemDetails(url, db) {
  var item = {
    price: 0,
    title: "",
    brand: "",
    sku: "",
    url: "",
    category: ""
  };

  return rp(url).then(function (html) {
    var $ = cheerio.load(html);
    item.price = $('input#adwordsTotalValue').first().attr('value');
    item.title = $('h1.pdp-main-ttl').text().trim();
    item.url = url;
    item.brand = $('meta[itemprop=brand]').attr('content');
    item.sku = $('meta[itemprop=sku]').attr('content');
    item.category = category;
    itemList.push(item);
    return itemList;
  });
}

function getPageLinks(url) {

  var linkBranch = rp(url).then(function (html) {
    var $ = cheerio.load(html);
    $('a.pdpLink').each(function (i, elem) {
      itemUrls.push($(elem).attr('href'));
    });
    var nextPage = $('a.icon-paging-right').first().attr('href');
    if (nextPage) {
      return new Promise(function (resolve) {
        setTimeout(function () {
          resolve(getPageLinks(nextPage));
        }, 10000);
      });
    } else {
      console.log("No next page");
      return;
    }
  });
  return linkBranch;
}

function crawlPage(db, page) {
  category = page.category;
  return getPageLinks(page.link).then(function () {
    //Search URLS -> Promise
    var pagePromises = itemUrls.map(function (url) {
      return new Promise(function (resolve) {
        setTimeout(function () {
          resolve(getItemDetails(url, db));
        }, 1000);
      });
    });
    return Promise.all(pagePromises);
  }).then(function (itemList) {
    return itemList;
  }).catch(function (e) {
    return console.error(e);
  });
}

exports.crawlSitemap = crawlSitemap;
exports.crawlPage = crawlPage;
//# sourceMappingURL=crawlerBC.js.map