'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
var request = require('request');
var cheerio = require('cheerio');
var _ = require('underscore');
var mj = require('./crawlerMJ.js');
var Promise = require('bluebird');

// Urls for testing
//const categoryUrls = ['http://www.moosejaw.com/moosejaw/shop/search_1-Person-Tents____','http://www.moosejaw.com/moosejaw/shop/navigation__Day-Hike-Packs_____']
// Final list of item pages to crawl

function scrape(db) {
  db.company.create({ name: "Moosejaw" });
  console.log("Scraper started");
  var catPages = mj.crawlSitemap('http://www.moosejaw.com/moosejaw/shop/content_sitemap____', 'ul.inter-ship');

  //pages are lines 0-78
  catPages.then(function (catList) {
    mj.crawlCatz(catList.slice(0, 78)).then(function (itemList) {

      Promise.each(itemList, function (item) {
        return mj.getItemDetails(db, item);
      });
      return itemList;
    });
  });
}

exports.scrape = scrape;
//# sourceMappingURL=scraper.js.map