const express = require('express');
const bodyParser = require('body-parser');
const _ = require('underscore');
const db = require('./db.js');
const middleware = require('./middleware.js')(db);

const app = express();
const PORT = process.env.PORT || 3000;

// GET /categories
app.use(bodyParser.json());

app.get('/', (req, res) => {
  // scrape(db);
  // db.sequelize.query("SELECT DISTINCT price FROM products",
  // { type: db.sequelize.QueryTypes.SELECT})
  // .then(function(prices) {
  //   console.log(prices)
  //   res.send("Success!")
  //   // We don't need spread here, since only the results will be returned for select queries
  // })
  res.send('What up');
});

// POST /users
app.post('/users', (req, res) => {
  const body = _.pick(req.body, 'email', 'password');

  db.user.create(body).then(user => res.json(user.toPublicJSON()), (e) => {
    res.status(400).json(e);
  });
});

// POST /users/login
app.post('/users/login', (req, res) => {
  const body = _.pick(req.body, 'email', 'password');
  let userInstance;

  db.user.authenticate(body).then((user) => {
    const token = user.generateToken('authentication');
    userInstance = user;
    return db.token.create({
      token,
    });
  }).then((tokenInstance) => {
    res.header('Auth', tokenInstance.get('token')).json(userInstance.toPublicJSON());
  }).catch(() => {
    res.status(401).send();
  });
});

//  DELETE /users/login
app.delete('/users/login', middleware.requireAuthentication, (req, res) => {
  req.token.destroy().then(() => {
    res.status(204).send();
  }).catch(() => {
    res.status(500).send();
  });
});


db.sequelize.sync({
  // force:true
}).then(() => {
  app.listen(PORT, () => {
    console.log(`Express server started at: http://localhost:${PORT}`);
  });
}).catch(e => console.log(e));
