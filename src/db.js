const Sequelize = require('sequelize');
const env = process.env.NODE_ENV || 'development';

let sequelize;

if (env === 'production') {
  sequelize = new Sequelize(process.env.DATABASE_URL, {
    dialect: 'postgres',
  });
} else {
  sequelize = new Sequelize(undefined, undefined, undefined, {
    dialect: 'sqlite',
    storage: `${__dirname}/data/dev-mj-products-api.sqlite`,
  });
}

const db = {};

db.product = sequelize.import(`${__dirname}/models/product.js`);
db.user = sequelize.import(`${__dirname}/models/user.js`);
db.token = sequelize.import(`${__dirname}/models/token.js`);
db.company = sequelize.import(`${__dirname}/models/company.js`);
db.sequelize = sequelize;
db.Sequelize = Sequelize;

db.product.belongsTo(db.company);
db.company.hasMany(db.product);

module.exports = db;
