import 'async';
const mj = require('./crawlerMJ.js');
const bc = require('./crawlerBC.js');
const db = require('../db.js');


// serializes Item page requests and inserts into DB
// Function(Array::array, async::function => items[], async::function => item) => Array::itemCounts
async function serializeCategoryCrawling(startPoints, asyncNavFunction, asyncItemFunction) {
  // Loop over category starting Urls and let await handle the flow control
  const itemsInCategory = [];
  for (let i = 0; i < startPoints.length; i++) {
    // Wait for crawler to get item urls from all category pages
    const items = await asyncNavFunction(startPoints[i], []);
    itemsInCategory.push(items.length);
    // Loop through each item in category
    for (let idx = 0; idx < items.length; idx++) {
      try {
        // Request item page and db results
        const item = await asyncItemFunction({ link: items[idx], category: startPoints[i].category }, db);
      } catch (error) {
        console.log(error);
      }
    }
  }

  // console.log(itemsInCategory.reduce((acc,elem,idx) => {
  //   return acc+elem;
  // }),0)

  return itemsInCategory;
}

async function scrape(db) {
  // db.company.findOrCreate({where: {name: "Moosejaw"}});
  // db.company.findOrCreate({where: {name: "Backcountry"}});
  console.log('Scraper started');

  // Grab starting urls for web crawlers
  const mjPages = await mj.crawlSitemap('http://www.moosejaw.com/moosejaw/shop/content_sitemap____');
  const bcPages = await bc.crawlSitemap({
    uri: 'http://www.backcountry.com/sc/popular-categories-sitemap',
    headers: {
      'user-agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36',
    },
  });

  // Crawl each sites category Endpoints
  // Slice mjPages to remove sale categories for now
  serializeCategoryCrawling(mjPages.slice(20, 78), mj.crawlCategory, mj.getItemDetails);
  serializeCategoryCrawling(bcPages, bc.crawlCategory, bc.getItemDetails);
}

db.sequelize.sync({
  // force:true
}).then(() => {
  console.log('Connected to db!');
  scrape(db);
}).catch(e => console.log(e));
