const rp = require('request-promise');
const cheerio = require('cheerio');
const Promise = require('bluebird');
const chalk = require('chalk');
const log = console.log;
import 'async';


// Get links to product categories from site page
function crawlSitemap(url) {
  return rp(url).then((html) => {
    const $ = cheerio.load(html);
    let cat,
      title,
      link;
    const pageList = [];

    $('ul.inter-ship').each((i, elem) => {
      const str = $(elem).prev('h3').text();
      cat = str.slice(0, str.length - 1);   // remove trailing :

      $(elem).find('a').each((j, el) => {
        const obj = { category: '', linkTitle: '', link: '' };
        obj.category = cat;
        obj.link = `http://www.moosejaw.com/moosejaw/shop/${$(el).attr('href')}`;
        pageList.push(obj);
      });
    });

    return pageList;
  });
}

// Requests page.link, scrapes item information, inserts into db
async function getItemDetails(page, db) {
  let item = {
    price: 0,
    title: '',
    brand: '',
    sku: '',
    url: '',
    category: '',
  };

  try {
    item = await rp(page.link).then((html) => {
      const $ = cheerio.load(html);
      item.price = parseFloat($('input#adwordsTotalValue').first().attr('value'));
      item.title = $('h1.pdp-main-ttl').text().trim();
      item.url = page.link;
      item.brand = $('meta[itemprop=brand]').attr('content');
      item.sku = parseInt($('meta[itemprop=sku]').attr('content'));
      item.category = page.category;

      return item;
    });

    return db.company.findOne({ where: { name: 'Moosejaw' } }).then((company) => {
      db.product.create(item).then((item) => {
        company.addProduct(item).then(product => product.reload()).catch(e => console.log(e));
      }).catch((e) => {
        if (e.errors[0].message == 'sku must be unique') {
          log(`${chalk.blue('Warning:')}Avoiding sku collision: #${e.fields.sku}`);
        } else {
          log('Error adding new item to product table...');
          log(e);
        }
      });
    }).catch(e => console.log(e));
  } catch (error) {
    console.log(error);
  }
}


// Iterate through each page of items collecting item urls as we go
function crawlCategory(pageObj, items) {
  return rp(pageObj.link).then((html) => {
    const $ = cheerio.load(html);
    $('a.pdpLink').map((i, elem) => {
      if (!items.includes($(elem).attr('href'))) { items.push($(elem).attr('href')); }
    });
    const nextPage = $('a.icon-paging-right').first().attr('href');
    if (nextPage) {
      pageObj.link = nextPage;

      return new Promise((resolve, reject) => {
        resolve(crawlCategory(pageObj, items));
      });
    }
    return items;
  });
}

export { crawlSitemap, crawlCategory, getItemDetails };
