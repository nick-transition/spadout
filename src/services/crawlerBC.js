const rp = require('request-promise');
const cheerio = require('cheerio');
const Promise = require('bluebird');
const chalk = require('chalk');
const log = console.log;
import 'async';

// Get list of product cateegories from sitemap
function crawlSitemap(url) {
  return rp(url).then((html) => {
    const $ = cheerio.load(html);
    let cat,
      title,
      link;
    const pageList = [];
    $('section.main td').each((i, elem) => {
      const clnStr = $(elem).find('a').text().replace(/^\s+/, '').replace(/\s+$/, '');
      if (clnStr != '') {
        const obj = { category: '', link: '' };
        obj.category = $(elem).find('a').text();
        obj.link = `http:${$(elem).find('a').attr('href')}`;
        pageList.push(obj);
      }
    });
    return pageList;
  });
}

async function getItemDetails(page, db) {
  let item = {
    price: 0,
    title: '',
    brand: '',
    sku: '',
    url: '',
    imageLink: '',
    category: '',
  };

  try {

    //Wait fo page request to resolve to item object
    item = await rp({
      uri: page.link,
      headers: {
        'user-agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36',
      },
    }).then((html) => {
      const $ = cheerio.load(html);
      item.imageLink = $('meta[property="og:image"]').attr('content');
      item.price = parseFloat($('meta[itemprop=price]').first().attr('content'));
      item.title = $('meta[property="og:title"]').attr('content');
      item.url = $('meta[property="og:url"]').attr('content');
      item.brand = $('img.product-brand__img').attr('alt');
      item.sku = parseInt($('li.product-details-accordion_item-number').text().slice(6)); // remove garbage text in front of sku
      item.category = page.category;

      return item;
    });

    //Return promise to find and place item in DB
    return db.company.findOne({ where: { name: 'Backcountry' } }).then((company) => {
      db.product.create(item).then((item) => {
        company.addProduct(item).then(product => product.reload()).catch(e => console.log(e));
      }).catch((e) => {
        log(`${chalk.red('ERROR:')} adding new item to product table...`);
        log(e);
      });
    }).catch(e => console.log(e));
  } catch (error) {
    console.log(error);
  }
}


// Iterate through each page of items collecting item urls as we go
function crawlCategory(pageObj, items) {

  return rp({
    uri: pageObj.link,
    headers: {
      'user-agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36',
    },
  }).then((html) => {
    const $ = cheerio.load(html);
    $('div.product a.ui-pl-link').map((i, elem) => {
      items.push(`https://www.backcountry.com/${$(elem).attr('href')}`);
    });
    const nextPage = $('li.pag-next a').attr('href'); // '/mens-shirts-new-arrivals?page=1'
    if (nextPage) {
      pageObj.link = `https://www.backcountry.com/${nextPage}`;

      return new Promise((resolve, reject) => {
        resolve(crawlCategory(pageObj, items));
      });
    }

    return items;
  });
}


export { crawlSitemap, crawlCategory, getItemDetails };
