const bcrypt = require('bcrypt');
const _ = require('underscore');
const cryptojs = require('crypto-js');
const jwt = require('jsonwebtoken');

module.exports = function (sequelize, DataTypes) {
  var user = sequelize.define('user', {
    email: {
      type: DataTypes.STRING,
      allowNull: false,
      unique: true,
      validate: {
        isEmail: true,
      },
    },
    salt: {
      type: DataTypes.STRING,
    },
    password_hash: {
      type: DataTypes.STRING,
    },
    password: {
      type: DataTypes.VIRTUAL,
      allowNUll: false,
      validate: {
        len: [7, 100],
      },
      set(value) {
        const salt = bcrypt.genSaltSync(10);
        const hashedPassword = bcrypt.hashSync(value, salt);

        this.setDataValue('password', value);
        this.setDataValue('salt', salt);
        this.setDataValue('password_hash', hashedPassword);
      },
    },
  }, {
    hooks: {
      beforeValidate(user, options) {
        if (typeof user.email === 'string') {
          user.email = user.email.toLowerCase();
        }
      },
    },
    classMethods: {
      authenticate(body) {
        return new Promise((resolve, reject) => {
          if (typeof body.email !== 'string' || typeof body.password !== 'string') {
            return reject();
          }

          user.findOne({
            where: {
              email: body.email,
            },
          }).then((user) => {
            if (!user || !bcrypt.compareSync(body.password, user.get('password_hash'))) {
              return reject();
            }

            resolve(user);
          }, (e) => {
            reject();
          });
        });
      },
      findByToken(token) {
        return new Promise((resolve, reject) => {
          try {
            const decodedJWT = jwt.verify(token, 'qwerty098');
            const bytes = cryptojs.AES.decrypt(decodedJWT.token, 'abc123!@#!');
            const tokenData = JSON.parse(bytes.toString(cryptojs.enc.Utf8));

            user.findById(tokenData.id).then((user) => {
              if (user) {
                resolve(user);
              } else {
                reject();
              }
            }, (e) => {
              reject();
            });
          } catch (e) {
            reject();
          }
        });
      },
    },
    instanceMethods: {
      toPublicJSON() {
        const json = this.toJSON();
        return _.omit(json, 'password', 'salt', 'password_hash');
      },
      generateToken(type) {
        if (!_.isString(type)) {
          return undefined;
        }

        try {
          const stringData = JSON.stringify({ id: this.get('id'), type });
          const encryptedData = cryptojs.AES.encrypt(stringData, 'abc123!@#!').toString();
          const token = jwt.sign({
            token: encryptedData,
          }, 'qwerty098');

          return token;
        } catch (e) {
          console.error(e);
          return undefined;
        }
      },
    },
  });
  // console.log(user);
  return user;
};
