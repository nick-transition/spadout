const cryptojs = require('crypto-js');

export default function (sequelize, DataTypes) {
  return sequelize.define('token', {
    token: {
      type: DataTypes.VIRTUAL,
      allowNUll: false,
      validate: {
        len: [1],
      },
      set(value) {
        const hash = cryptojs.MD5(value).toString();

        this.setDataValue('token', value);
        this.setDataValue('tokenHash', hash);
      },
    },
    tokenHash: DataTypes.STRING,
  });
}
