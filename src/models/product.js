module.exports = function (sequelize, DataTypes) {
  return sequelize.define('product', {
    price: {
      type: DataTypes.DECIMAL(10, 2),
      allowNull: false,
      validate: {
        isDecimal: true,
      },
    },
    title: {
      type: DataTypes.STRING,
      allowNull: false,
      validate: {
        len: [2, 240],
      },
    },
    category: {
      type: DataTypes.STRING,
      allowNull: false,
      validate: {
        len: [2, 240],
      },
    },
    brand: {
      type: DataTypes.STRING,
      allowNull: false,
      validate: {
        len: [2, 240],
      },
    },
    sku: {
      type: DataTypes.INTEGER,
      allowNull: false,
      unique: true,
      validate: {
        len: [0, 10],
      },
    },
    url: {
      type: DataTypes.STRING,
      allowNull: false,
      validate: {
        len: [5, 450],
      },
    },
    imageLink: {
      type: DataTypes.STRING,
      allowNull: true,
      validate: {
        len: [5, 450],
      },
    },
  });
};
